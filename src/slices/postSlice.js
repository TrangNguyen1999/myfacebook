/* eslint-disable prettier/prettier */
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import postApi from '../apis/postApi';
import * as responses from './../constants/responses';

export const getListPostsRequest = createAsyncThunk('post/getListPostsRequest', async params => {
  try {
    const response = await postApi.getListPosts(params);
    return response;
  } catch (error) {
    console.log('Error at getListPostsRequest:', error.message);
  }
});

export const likeRequest = createAsyncThunk('post/likeRequest', async params => {
  try {
    const response = await postApi.like(params);
    if (response.code === responses.OK) {
      return {
        code: response.code,
        id: 1,// params.id,
        like: parseInt(response.data.like, 10),
      };
    }
  } catch (error) {
    console.log('Error at likeRequest:', error.message);
  }
});

const post = createSlice({
  name: 'post',
  initialState: {
    loadingGetListPostsRequest: false,
    listPostsMain: [],
    lastId: null,
  },
  reducers: {

  },
  extraReducers: {
    [getListPostsRequest.pending]: state => {
      state.loadingGetListPostsRequest = true;
    },
    [getListPostsRequest.rejected]: () => {

    },
    [getListPostsRequest.fulfilled]: (state, action) => {
      state.loadingGetListPostsRequest = false;
      if (action.payload.code === responses.OK) {
        state.listPostsMain = action.payload.data.posts;
      }
    },

    [likeRequest.pending]: state => {

    },
    [likeRequest.rejected]: () => {

    },
    [likeRequest.fulfilled]: (state, action) => {
      console.log(action.payload);
      if (action.payload.code === responses.OK) {
        state.listPostsMain[1].like = action.payload.like;
        state.listPostsMain[1].is_liked = !state.listPostsMain[1].is_liked;
      }
    },
  },
});

const { reducer, actions } = post;

// export const {

// } = actions;
export default reducer;

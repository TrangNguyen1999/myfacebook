/* eslint-disable prettier/prettier */
import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import PostWithImageList from '../../components/post/PostWIthImageList';

function PostWithImageListScreen({ navigation }) {
  return (
    <ScrollView>
      <PostWithImageList
      like={'1'}
        comment={''}
        is_liked={false}
        image={[
          {
            id: 'one',
            url: 'https://picsum.photos/id/1000/400/100',
          },
          {
            id: 'two',
            url: 'https://picsum.photos/id/1001/5616/3744',
          },
          {
            id: 'three',
            url: 'https://picsum.photos/id/1010/5184/3456',
          },
        ]}
        described={'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been'}
        author={{
          id: 'demoId',
          name: 'Trang Nguyen',
          avatar: 'https://picsum.photos/id/1010/5184/3456',
        }}
        created={1605275104}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({

});

export default PostWithImageListScreen;

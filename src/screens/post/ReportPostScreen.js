/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ListProblem from '../../components/reportPost/ListProblem';
import ListSubProblem from '../../components/reportPost/ListSubProblem';
import { REPORT_POST } from '../../constants/reportPost';
import * as colors from './../../constants/colors';

function ReportPostScreen({ navigation, route }) {
  const [problem, setProblem] = useState(null);
  const [subProblem, setSubProblem] = useState(null);

  const handleProblemPress = (id) => {
    const index = parseInt(id, 10);
    setProblem(REPORT_POST[index]);
  };

  const handleSubProblemPress = (subId) => {
    const index = parseInt(subId, 10);
    setSubProblem(problem?.details[index]);
  };

  return (
    <View style={styles.container}>
      <View style={styles.hearder}>
        <Text style={styles.textHeader}>Báo cáo</Text>
        <View style={styles.close}>
          <TouchableOpacity>
            <Ionicons name="close" color={colors.grey900} size={24} />
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView>
        <Text>Vui lòng chọn vấn đề để tiếp tục</Text>
        <Text>Bạn có thể báo cáo bài viết sau khi chọn vấn đề</Text>
        <View style={styles.problems}>
          <ListProblem
            activeId={problem?.id}
            handleProblemPress={handleProblemPress}
          />
          <ListSubProblem
            activeId={subProblem?.id}
            handleProblemPress={handleSubProblemPress}
          />
        </View>
      </ScrollView>
      <View>
        <TouchableOpacity>
          <View>
            <Text>Tiếp</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
  },
  hearder: {

  },
  textHeader: {
    textAlign: 'center',
  },
  close: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  problems: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});

export default ReportPostScreen;

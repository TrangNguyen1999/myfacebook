/* eslint-disable prettier/prettier */
import React from 'react';
import { ActivityIndicator, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { logoutRequest } from '../slices/authSlice';
import * as colors from './../constants/colors';

function SettingScreen() {
  console.log('SettingScreen render');

  const tokenMain = useSelector(state => state.auth.tokenMain);
  const loadingLogoutRequest = useSelector(state => state.auth.loadingLogoutRequest);
  const dispatch = useDispatch();

  const onLogout = () => {
    dispatch(logoutRequest({
      token: tokenMain,
    }));
  };

  return (
    <View>
      <Text>Setting</Text>
      <TouchableOpacity
        onPress={onLogout}
      >
        <View>
          <Text>logout</Text>
        </View>
      </TouchableOpacity>
      <Modal
        visible={loadingLogoutRequest}
        transparent={true}
      >
        <View style={styles.modal}>
          <View style={styles.modalView}>
            <ActivityIndicator size="small" color={colors.grey700} />
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
  },
  modal: {
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
  },
  modalView: {
    backgroundColor: colors.white,
    margin: 30,
    padding: 30,
  },
});

export default SettingScreen;

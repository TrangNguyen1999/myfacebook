/* eslint-disable prettier/prettier */
import React, { useEffect, useRef } from 'react';
import { ActivityIndicator, FlatList, Modal, StyleSheet, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import BottomSheet from 'reanimated-bottom-sheet';
import PostOptionOfMe from '../components/post/bottomSheet/PostOptionOfMe';
import PostOptionOfOther from '../components/post/bottomSheet/PostOptionOfOther';
import Post from '../components/post/Post';
import PostCard from '../components/post/PostCard';
import { getListPostsRequest, likeRequest } from '../slices/postSlice';
import * as colors from './../constants/colors';

function HomeScreen({ navigation }) {
  console.log('HomeScreen render');

  const sheetPostOptionOfMeRef = useRef(null);
  const sheetPostOptionOfOtherRef = useRef(null);

  const avatarMain = useSelector(state => state.auth.avatarMain);
  const tokenMain = useSelector(state => state.auth.tokenMain);

  const listPostsMain = useSelector(state => state.post.listPostsMain);
  const loadingGetListPostsRequest = useSelector(state => state.post.loadingGetListPostsRequest);

  const dispatch = useDispatch();

  // ok
  const handleCreatePost = () => {
    navigation.navigate('CreatePostScreen');
  };

  const handlePostOptionClick = (option) => {
    if (option === 'ME') {
      sheetPostOptionOfMeRef.current.snapTo(0);
    } else if (option === 'OTHER') {
      sheetPostOptionOfOtherRef.current.snapTo(0);
    }
  };

  const handleLikeClick = () => {
    console.log('handleLikeClick');
    dispatch(likeRequest());
  };

  const handleCommentClick = () => {
    console.log('handleCommentClick');
  };

  useEffect(() => {
    dispatch(getListPostsRequest({
      token: tokenMain,
      user_id: '',
      // ...
      // last_id
      index: '0',
      count: '20',
    }));
  }, []);

  const renderItem = ({ item }) => (
    <View style={styles.post}>
      <Post
        name={item.name}
        author={item.author}
        state={item.state}
        created={item.created}
        described={item.described}
        image={item.image}
        video={item.video}
        like={item.like}
        comment={item.comment}
        is_liked={item.is_liked}
        is_blocked={item.is_blocked}
        can_comment={item.can_comment}
        can_edit={item.can_edit}
        banned={item.banned}
        handlePostOptionClick={handlePostOptionClick}
        handleLikeClick={handleLikeClick}
        handleCommentClick={handleCommentClick}
      />
    </View>
  );

  const turnOffNotification = () => {

  };

  const savePost = () => {

  };

  const deletePost = () => {

  };

  const editPost = () => {

  };

  const copyLink = () => {

  };

  const reportPost = () => {
    navigation.navigate('ReportPostScreen');
  };

  const turnOnNotification = () => {

  };

  const renderPostOptionOfMe = () => (
    <PostOptionOfMe
      turnOffNotification={turnOffNotification}
      savePost={savePost}
      deletePost={deletePost}
      editPost={editPost}
      copyLink={copyLink}
    />
  );

  const renderPostOptionOfOther = () => (
    <PostOptionOfOther
      savePost={savePost}
      reportPost={reportPost}
      turnOnNotification={turnOnNotification}
      copyLink={copyLink}
    />
  );

  return (
    <>
      <View style={styles.container}>
        <FlatList
          ListHeaderComponent={() => (
            <PostCard
              avatar={avatarMain}
              handleCreatePost={handleCreatePost}
            />
          )}
          data={listPostsMain}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
        <Modal
          visible={loadingGetListPostsRequest}
          transparent={true}
        >
          <View style={styles.modal}>
            <View style={styles.modalView}>
              <ActivityIndicator size="small" color={colors.grey700} />
            </View>
          </View>
        </Modal>
      </View>
      <BottomSheet
        ref={sheetPostOptionOfMeRef}
        snapPoints={[230, 0]}
        initialSnap={1}
        enabledContentTapInteraction={false}
        renderContent={renderPostOptionOfMe}
      />
      <BottomSheet
        ref={sheetPostOptionOfOtherRef}
        snapPoints={[184, 0]}
        initialSnap={1}
        enabledContentTapInteraction={false}
        renderContent={renderPostOptionOfOther}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.blueGrey100,
    flex: 1,
  },
  post: {
    marginTop: 10,
  },
  modal: {
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
  },
  modalView: {
    backgroundColor: colors.white,
    margin: 30,
    padding: 30,
  },
});

export default HomeScreen;

/* eslint-disable prettier/prettier */
import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import * as colors from './../constants/colors';

function SplashScreen() {
  return (
    <View style={styles.container}>
      <Image
        source={require('./../../assets/images/facebookIcon.jpg')}
        style={styles.facebookIcon}
      />
      <Image
        source={require('./../../assets/images/splash.jpg')}
        style={styles.splash}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.white,
    flex: 1,
  },
  facebookIcon: {
    height: 80,
    marginTop: 300,
    width: 80,
  },
  splash: {
    height: 40,
    marginTop: 230,
    width: 160,
  },
});

export default SplashScreen;

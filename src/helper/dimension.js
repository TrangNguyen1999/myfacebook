/* eslint-disable prettier/prettier */
import { Dimensions } from 'react-native';

export const WINDOW_WIDTH = Dimensions.get('window').width;

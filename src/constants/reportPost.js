/* eslint-disable prettier/prettier */
export const REPORT_POST = [
  {
    id: '0',
    name: 'Ảnh khỏa thân',
    details: [
      {
        id: '0',
        name: 'Ảnh khỏa thân người lớn',
      },
      {
        id: '1',
        name: 'Gợi dục',
      },
      {
        id: '2',
        name: 'Hoạt động tình dục',
      },
      {
        id: '3',
        name: 'Bóc lột tình dục',
      },
      {
        id: '4',
        name: 'Dịch vụ tình dục',
      },
      {
        id: '5',
        name: 'Liên quan đến trẻ em',
      },
      {
        id: '6',
        name: 'Chia sẻ hình ảnh riêng tư',
      },
    ],
  },
  {
    id: '1',
    name: 'Bạo lực',
    details: [
      {
        id: '0',
        name: 'Hình ảnh bạo lực',
      },
      {
        id: '1',
        name: 'Tử vong hoặc bị thương nặng',
      },
      {
        id: '2',
        name: 'Mối đe dọa bạo lực',
      },
      {
        id: '3',
        name: 'Ngược đãi động vật',
      },
      {
        id: '4',
        name: 'Vấn đề khác',
      },
    ],
  },
  {
    id: '2',
    name: 'Quấy rối',
  },
  {
    id: '3',
    name: 'Tự tử/Tự gây thương tích',
  },
  {
    id: '4',
    name: 'Tin giả',
  },
  {
    id: '5',
    name: 'Spam',
  },
  {
    id: '6',
    name: 'Bán hàng trái phép',
  },
  {
    id: '7',
    name: 'Ngôn từ gây thù ghét',
  },
  {
    id: '8',
    name: 'Khủng bố',
  },
];

/* eslint-disable prettier/prettier */
import queryString from 'query-string';
import axiosClient from './axiosClient';

const postApi = {
  addPost: params => {
    // const url = `/link?${queryString.stringify(params)}`;
    // return axiosClient.post(url);
    // token o
    // image x [file, file,...]
    // video x file
    // described x
    // status x
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          code: '1000',
          data: {
            id: 'demoId',
            url: 'demoUrl', // co the de trong
          },
        });
      }, 1000);
    });
  },
  getPost: params => {
    // const url = `/link?${queryString.stringify(params)}`;
    // return axiosClient.post(url);
    // token o type?
    // id o type? id cua bai
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          code: '1000',
          data: {
            id: 'demoId',
            described: 'demoDescribed',
            created: 'demoCreated',
            modified: 'demoModified',
            like: 'demoLike', // so luong like
            comment: 'demoComment', // so luong comment
            is_liked: '1', // 1 da like, 0 chua like
            image: [ // x
              {
                id: 'demoId',
                url: 'demoUrl',
              },
              {
                id: 'demoId',
                url: 'demoUrl',
              },
            ],
            video: { // x
              url: 'demoUrl',
              thumb: 'demoThumb',
            },
            author: {
              id: 'demoId',
              name: 'demoName',
              avatar: 'demoAvatar',
            },
            state: 'demoState',
            is_blocked: '',
            can_edit: '',
            banned: '',
            can_comment: '', // x
          },
        });
      }, 1000);
    });
  },
  editPost: params => {
    // const url = `/link?${queryString.stringify(params)}`;
    // return axiosClient.post(url);
    // token o
    // id o int id bai
    // ...
  },
  like: params => {
    // const url = `/link?${queryString.stringify(params)}`;
    // return axiosClient.post(url);
    // token o
    // id int o id bai viet
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          code: '1000',
          data: {
            like: '3', // so like hien tai
          },
        });
      }, 1000);
    });
  },
  getListPosts: parmas => {
    // const url = `/link?${queryString.stringify(params)}`;
    // return axiosClient.post(url);
    // token x
    // user_id x
    // in_campaign x ???
    // campaign_id x ???
    // latitude x toa do nguoi dung
    // longitude x toa do nguoi dung
    // last_id x last_id tra ve lan truoc
    // index o index start select
    // count o so bai trong 1 lan gui yeu cau
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          code: '1000',
          data: {
            posts: [
              {
                id: '1',
                name: '',
                image: '',
                video: '',
                described: 'post 1',
                created: '1605275104',
                like: '',
                comment: '',
                is_liked: '',
                is_blocked: '',
                can_comment: '',
                can_edit: '',
                banned: '',
                state: '',
                author: {
                  id: '1',
                  name: 'name 1',
                  avatar: 'https://picsum.photos/id/1010/5184/3456',
                },
              },
              {
                id: '2',
                name: '',
                image: '',
                video: '',
                described: 'post 2',
                created: '1605275104',
                like: '2',
                comment: '100',
                is_liked: false,
                is_blocked: '',
                can_comment: '',
                can_edit: '',
                banned: '',
                state: '',
                author: {
                  id: '2',
                  name: 'name 2',
                  avatar: 'https://picsum.photos/id/1010/5184/3456',
                },
              },
              {
                id: '3',
                name: '',
                image: '',
                video: '',
                described: 'post 3',
                created: '1605275104',
                like: '',
                comment: '',
                is_liked: '',
                is_blocked: '',
                can_comment: '',
                can_edit: '',
                banned: '',
                state: '',
                author: {
                  id: '3',
                  name: 'name 3',
                  avatar: 'https://picsum.photos/id/1010/5184/3456',
                },
              },
              {
                id: '4',
                name: '',
                image: '',
                video: '',
                described: 'post 4',
                created: '1605275104',
                like: '',
                comment: '',
                is_liked: '',
                is_blocked: '',
                can_comment: '',
                can_edit: '',
                banned: '',
                state: '',
                author: {
                  id: '4',
                  name: 'name 4',
                  avatar: 'https://picsum.photos/id/1010/5184/3456',
                },
              },
              {
                id: '5',
                name: '',
                image: '',
                video: '',
                described: 'post 5',
                created: '1605275104',
                like: '',
                comment: '',
                is_liked: '',
                is_blocked: '',
                can_comment: '',
                can_edit: '',
                banned: '',
                state: '',
                author: {
                  id: '5',
                  name: 'name 5',
                  avatar: 'https://picsum.photos/id/1010/5184/3456',
                },
              },
              {
                id: '6',
                name: '',
                image: '',
                video: '',
                described: 'post 6',
                created: '1605275104',
                like: '',
                comment: '',
                is_liked: '',
                is_blocked: '',
                can_comment: '',
                can_edit: '',
                banned: '',
                state: '',
                author: {
                  id: '6',
                  name: 'name 6',
                  avatar: 'https://picsum.photos/id/1010/5184/3456',
                },
              },
            ],
            new_items: 'demo', // so bai viet moi dang
            last_id: 'demo', // id bai viet cuoi
          },
          in_campaign: 'demoInCampaign', // cap nhat in_campaign
          campaign_id: 'demoCampaignId', // cap nhat id cua campaign
        });
      }, 1000);
    });
  },
};

export default postApi;

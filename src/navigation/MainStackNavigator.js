/* eslint-disable prettier/prettier */
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import NavBar from '../components/NavBar';
import CreatePostScreen from '../screens/post/CreatePostScreen';
import FeelTabNavigator from '../screens/post/navigation/FeelTabNavigator';
import PostWithImageListScreen from '../screens/post/PostWithImageListScreen';
import ReportPostScreen from '../screens/post/ReportPostScreen';
import TopTabNavigator from './TopTabNavigator';

const Stack = createStackNavigator();

function MainStackNavigator() {
  return (
    <Stack.Navigator initialRouteName="TopTabNavigator">
      <Stack.Screen
        name="TopTabNavigator"
        component={TopTabNavigator}
        options={{
          headerTitle: () => <NavBar />,
        }}
      />
      <Stack.Screen
        name="CreatePostScreen"
        component={CreatePostScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="FeelTabNavigator"
        component={FeelTabNavigator}
        options={{
          title: 'Cảm xúc/Hoạt động',
        }}
      />
      <Stack.Screen
        name="ReportPostScreen"
        component={ReportPostScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="PostWithImageListScreen"
        component={PostWithImageListScreen}
        options={{
          title: 'Chi tiết bài viết',
        }}
      />
    </Stack.Navigator>
  );
}

export default MainStackNavigator;

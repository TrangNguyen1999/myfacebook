/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import * as colors from './../../constants/colors';
import Describe from './Describe';
import DisplayImageGrid from './DisplayImageGrid';
import LikeCommentAmountBar from './LikeCommentAmountBar';
import LikeCommentButtonBar from './LikeCommentButtonBar';
import { Modal } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import Author from './Author';
import DisplayImageList from './DisplayImageList';

function PostWithImageList({
  described,
  created,
  modified,
  like,
  comment,
  is_liked,
  image,
  video,
  author,
  state,
  is_blocked,
  can_edit,
  banned,
  can_comment,
}) {
  const [likeAmount, setLikeAmount] = useState(parseInt(like, 10));
  const [liked, setLiked] = useState(is_liked);

  const handleAmountBarClick = () => {

  };
  const handleLikeClick = () => {
    if (liked) {
      setLikeAmount(likeAmount - 1);
    } else {
      setLikeAmount(likeAmount + 1);
    }
    setLiked(!liked);
    // dispath ...
  };
  const handleCommentClick = () => {

  };

  return (
    <View style={styles.container}>
      <Author
        author={author}
        created={created}
        state={state}
      />
      <Describe
        describe={described}
      />
      <LikeCommentAmountBar
        like={likeAmount}
        comment={comment}
        liked={liked}
        handleAmountBarClick={handleAmountBarClick}
      />
      <LikeCommentButtonBar
        liked={liked}
        handleLikeClick={handleLikeClick}
        handleCommentClick={handleCommentClick}
      />
      <DisplayImageList
        images={image}
      />
      {/* <Modal visible={true} transparent={true}>
        <ImageViewer
        imageUrls={[
          {
            url: 'https://picsum.photos/id/1000/5626/3635',
          },
          {
            url: 'https://picsum.photos/id/1001/5616/3744',
          },
          {
            url: 'https://picsum.photos/id/1010/5184/3456',
          },
        ]}
        />
      </Modal> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  },
});

export default PostWithImageList;

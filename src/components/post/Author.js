/* eslint-disable prettier/prettier */
import { Thumbnail } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { timeAgo } from '../../helper/timeAgo';
import * as colors from './../../constants/colors';

function Author({ author, created, state, handlePostOptionClick }) {
  return (
    <View style={styles.container}>
      <View style={styles.viewLeft}>
        <Thumbnail
          source={{ uri: author.avatar }}
          style={styles.avatar}
        />
        {author.online && <Ionicons style={styles.onlineState} name="ellipse" color={colors.lightGreenA700} size={12} />}
      </View>
      <View style={styles.viewMid}>
        <View>
          <Text style={styles.text}>
            <Text style={styles.username}>{author.name}</Text> dang cam thay hanh phuc cung ai do
          </Text>
        </View>
        <View>
          <Text style={styles.text}>{timeAgo(created)}</Text>
        </View>
      </View>
      <View style={styles.viewRight}>
        <TouchableOpacity
          onPress={() => {
            if (false) {
              handlePostOptionClick('ME');
            } else {
              handlePostOptionClick('OTHER');
            }
          }}
        >
          <Ionicons name="ellipsis-horizontal" color={colors.grey900} size={20} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 8,
  },
  viewLeft: {
    flexBasis: 3,
    flexGrow: 3,
  },
  viewMid: {
    flexBasis: 18,
    flexGrow: 18,
  },
  viewRight: {
    flexBasis: 2,
    flexGrow: 2,
  },
  avatar: {
    height: 40,
    width: 40,
  },
  text: {
    color: colors.grey900,
    fontSize: 16,
  },
  username: {
    color: colors.grey900,
    fontSize: 16,
    fontWeight: 'bold',
  },
  onlineState: {
    position: 'absolute',
    right: 10,
    top: 28,
  },
});

export default Author;

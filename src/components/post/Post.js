/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, View } from 'react-native';
import * as colors from './../../constants/colors';
import Author from './Author';
import Describe from './Describe';
import DisplayImageGrid from './DisplayImageGrid';
import LikeCommentAmountBar from './LikeCommentAmountBar';
import LikeCommentButtonBar from './LikeCommentButtonBar';
import Video from './Video';

function Post({
  name,
  author,
  state,
  created,
  modified,
  described,
  image,
  video,
  like,
  comment,
  is_liked,
  is_blocked,
  can_comment,
  can_edit,
  banned,
  handlePostOptionClick,
  handleImageClick,
  handleVideoClick,
  handleAmountBarClick,
  handleLikeClick,
  handleCommentClick,
}) {
  console.log(`post ${author.name} render`);
  return (
    <View style={styles.container}>
      <Author
        author={author}
        created={created}
        state={state}
        handlePostOptionClick={handlePostOptionClick}
      />
      <Describe
        describe={described}
      />
      <DisplayImageGrid
        images={image}
      />
      <Video
        video={video}
      />
      <LikeCommentAmountBar
        like={like}
        comment={comment}
        liked={is_liked}
        handleAmountBarClick={handleAmountBarClick}
      />
      <LikeCommentButtonBar
        liked={is_liked}
        handleLikeClick={handleLikeClick}
        handleCommentClick={handleCommentClick}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  },
});

export default Post;

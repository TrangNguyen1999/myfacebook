/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';
import { WINDOW_WIDTH } from '../../helper/dimension';
import * as colors from './../../constants/colors';

function DisplayImageList({ images }) {
  if (!images) {
    return null;
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity>
        <AutoHeightImage
          source={{ uri: images[0].url }}
          width={WINDOW_WIDTH}
          style={styles.image}
        />
      </TouchableOpacity>
      {images[1] && (
        <TouchableOpacity>
          <AutoHeightImage
            source={{ uri: images[1].url }}
            width={WINDOW_WIDTH}
            style={styles.image}
          />
        </TouchableOpacity>
      )}
      {images[2] && (
        <TouchableOpacity>
          <AutoHeightImage
            source={{ uri: images[2].url }}
            width={WINDOW_WIDTH}
            style={styles.image}
          />
        </TouchableOpacity>
      )}
      {images[3] && (
        <TouchableOpacity>
          <AutoHeightImage
            source={{ uri: images[3].url }}
            width={WINDOW_WIDTH}
            style={styles.image}
          />
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.blueGrey100,
  },
  image: {
    marginTop: 5,
  },
});

export default DisplayImageList;

/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from './../../constants/colors';

function BottomSheetOption({ icon, title, description, handler }) {
  return (
    <TouchableOpacity
      onPress={handler}
    >
      <View style={styles.container}>
        <View style={styles.icon}>
          <Ionicons name={icon} color={colors.grey700} size={24} />
        </View>
        {description ? (
          <View>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.description}>{description}</Text>
          </View>
        ) : <Text style={styles.title}>{title}</Text>}
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  icon: {
    padding: 10,
  },
  title: {
    color: colors.grey900,
    fontSize: 16,
  },
  description: {
    color: colors.grey700,
  },
});

export default BottomSheetOption;

/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet } from 'react-native';
import { REPORT_POST } from '../../constants/reportPost';
import Problem from './Problem';

function ListProblem({ activeId, handleProblemPress }) {
  let listProblemComponent = REPORT_POST.map((value) => (
    <Problem
      key={value.id}
      id={value.id}
      name={value.name}
      isActive={activeId === value.id}
      handleProblemPress={handleProblemPress}
    />
  ));

  return (
    <>
    {listProblemComponent}
    </>
  );
}

const styles = StyleSheet.create({

});

export default ListProblem;

/* eslint-disable prettier/prettier */
import { configureStore } from '@reduxjs/toolkit';
import authReducer from '../slices/authSlice';
import postReducer from '../slices/postSlice';

const rootReducer = {
  auth: authReducer,
  post: postReducer,
};

const store = configureStore({
  reducer: rootReducer,
});

export default store;
